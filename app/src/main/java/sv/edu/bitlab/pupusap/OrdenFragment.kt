package sv.edu.bitlab.pupusap

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.text.Layout
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import java.text.DecimalFormat


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
const val QUESO = 0//3
const val FRIJOLES = 1//4
const val REVUELTAS = 2//5
const val QUESO_MAIZ = 3//3
const val FRIJOLES_MAIZ = 4//4
const val REVUELTAS_MAIZ = 5//5
const val CONTADOR_ARROZ = "ARROZ"
const val CONTADOR_MAIZ = "MAIZ"
const val VALOR_PUPUSA = 0.5F
const val FRAGMENT_TAG = "FRAGMENT_TAG"
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PruebaFragment.PruebaFragmentListener] interface
 * to handle interaction events.
 * Use the [PruebaFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
3
class PruebaFragment : Fragment() {
  // TODO: Rename and change types of parameters
  val lineItemsIDs = arrayOf(
    arrayOf(R.id.lineItemDetail1, R.id.lineItemPrice1),
    arrayOf(R.id.lineItemDetail2, R.id.lineItemPrice2),
    arrayOf(R.id.lineItemDetail3, R.id.lineItemPrice3),
    arrayOf(R.id.lineItemDetail4, R.id.lineItemPrice4),
    arrayOf(R.id.lineItemDetail5, R.id.lineItemPrice5),
    arrayOf(R.id.lineItemDetail6, R.id.lineItemPrice6)
  )

  var text:TextView? = null
  var arroz = arrayListOf<Int>()
  var maiz = arrayListOf<Int>()
  private var listener: PruebaFragmentListener? = null
  private var inflater: LayoutInflater? = null
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    arguments?.let {
      arroz = it.getIntegerArrayList(CONTADOR_ARROZ)!!
      maiz = it.getIntegerArrayList(CONTADOR_MAIZ)!!
    }
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    displayDetalle()
    this.inflater = inflater
    return inflater.inflate(R.layout.fragment_prueaba, container, false)
  }
  fun displayDetalle() {


  }
  fun getDescripcion(index: Int): String {
    return when(index){
      DetalleOrdeActivity.QUESO -> "Queso de arroz"
      DetalleOrdeActivity.FRIJOLES -> "Frijol con queso de arroz"
      DetalleOrdeActivity.REVUELTAS -> "Revueltas de arroz"
      DetalleOrdeActivity.QUESO_MAIZ -> "Queso de maiz"
      DetalleOrdeActivity.FRIJOLES_MAIZ -> "Frijol con queso de maiz"
      DetalleOrdeActivity.REVUELTAS_MAIZ -> "Revueltas de maiz"
      else -> throw RuntimeException("Pupusa no soportada")
    }
  }
  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    var boton_enviar_pedido = view.findViewById<Button>(R.id.button)
    boton_enviar_pedido.setOnClickListener{

    }
    var arr = arroz!! + maiz!!
    var total = 0.0f
    for((index, contador) in  arr.withIndex()){
      val ids = lineItemsIDs[index]
      val detailTexview = view.findViewById<TextView>(ids[0])
      val priceTextView= view.findViewById<TextView>(ids[1])
      if(contador > 0){
        val totalUnidad = contador * DetalleOrdeActivity.VALOR_PUPUSA
        val descripcion = getDescripcion(index)
        detailTexview.text = getString(R.string.pupusa_line_item_description,
          contador, descripcion)
        total += totalUnidad
        val precio = DecimalFormat("$#0.00").format(totalUnidad)
        priceTextView.text = precio
      } else{
        detailTexview.visibility = View.GONE
        priceTextView.visibility = View.GONE
      }
    }
    val totalPrecio = view!!.findViewById<TextView>(R.id.lineItemPriceTotal)
    val precio = DecimalFormat("$#0.00").format(total)
    totalPrecio.text = precio
  }

  // TODO: Rename method, update argument and hook method into UI event
  fun onButtonPressed(uri: Uri) {
    listener?.onFragmentInteraction(uri)
  }
  override fun onAttach(context: Context) {
    super.onAttach(context)
    fun getDescripcion(index: Int): String {
      return when(index){
        DetalleOrdeActivity.QUESO -> "Queso de arroz"
        DetalleOrdeActivity.FRIJOLES -> "Frijol con queso de arroz"
        DetalleOrdeActivity.REVUELTAS -> "Revueltas de arroz"
        DetalleOrdeActivity.QUESO_MAIZ -> "Queso de maiz"
        DetalleOrdeActivity.FRIJOLES_MAIZ -> "Frijol con queso de maiz"
        DetalleOrdeActivity.REVUELTAS_MAIZ -> "Revueltas de maiz"
        else -> throw RuntimeException("Pupusa no soportada")
      }
    }
    if (context is PruebaFragmentListener) {
      listener = context
    } else {
      throw RuntimeException(context.toString() + " must implement PruebaFragmentListener")
    }
  }

  override fun onDetach() {
    super.onDetach()
    listener = null
  }

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   *
   *
   * See the Android Training lesson [Communicating with Other Fragments]
   * (http://developer.android.com/training/basics/fragments/communicating.html)
   * for more information.
   */
  interface PruebaFragmentListener {
    // TODO: Update argument type and name
    fun onFragmentInteraction(uri: Uri)
  }

  companion object {

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PruebaFragment.
     */
    // TODO: Rename and change types and number of parameters

    @JvmStatic
    fun newInstance(arroz:ArrayList<Int>, maiz: ArrayList<Int>) =
      PruebaFragment().apply {
        arguments = Bundle().apply {
          putIntegerArrayList(CONTADOR_ARROZ, arroz)
          putIntegerArrayList(CONTADOR_MAIZ, maiz)
        }
      }
    fun getDescripcion(index: Int): String {
      return when(index){
        DetalleOrdeActivity.QUESO -> "Queso de arroz"
        DetalleOrdeActivity.FRIJOLES -> "Frijol con queso de arroz"
        DetalleOrdeActivity.REVUELTAS -> "Revueltas de arroz"
        DetalleOrdeActivity.QUESO_MAIZ -> "Queso de maiz"
        DetalleOrdeActivity.FRIJOLES_MAIZ -> "Frijol con queso de maiz"
        DetalleOrdeActivity.REVUELTAS_MAIZ -> "Revueltas de maiz"
        else -> throw RuntimeException("Pupusa no soportada")
      }
    }
    private var arroz = arrayListOf<Int>()
    var maiz = arrayListOf<Int>()

  }
}
